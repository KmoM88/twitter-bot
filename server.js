// Init the environment variables and server configurations
require("dotenv").config();

// Import the required packages
const twitController = require("./twitter-bot/twit_controller");
const frases = require("./twitter-bot/frases");

let timemin = 60*60*2
let timems = 3600*timemin

async function retweetAnswerOn(account) {
  // console.log(frases)
  try {
    // Set up a user stream tracking mentions to username
    await twitController.streamCreate(account);
    // Now looking for tweet events
    // await twitController.streamOn();
  } catch (err1) {
    console.error(err1);
  }
}

async function periodic() {
  try {
    let frase = frases[Math.floor(Math.random() * frases.length)];
    // console.log(frase)
    const data = await twitController.postTweet(
      frase.frase + " " + frase.autor
    );
    // console.log(data)
  } catch (error) {
    console.error(error);
  }
}

async function hashRetweet(hash, count) {
  const data = await twitController.getHash(hash, count);
  const tweets = data.statuses;
  console.log("We got the tweets", tweets.length);
  for await (let tweet of tweets) {
    try {
      await twitController.postReTweet(tweet.id_str);
      //console.log(data)
      console.log("successful retweet ", tweet.id_str);
    } catch (err) {
      console.error(err);
      console.log("unsuccessful retweet ", tweet.id_str, err);
    }
  }
}

console.log("Starting twitter bot...");
retweetAnswerOn("@feder0ssi");
// hashRetweet("futurock", 5);
// periodic()
// setInterval(periodic, timems);
