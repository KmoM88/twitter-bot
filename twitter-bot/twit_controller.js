const client = require("./twit");
const input = require("./input");
let fs = require("fs");

async function tweetEvent(tweet) {
  //   let file = "./input.txt";
  // Who is this in reply to?
  // console.log(tweet);
  // What is the text?
  let txt = tweet.text;
  // Get rid of the @ mention
  let twit_text = txt.replace(/@feder0ssi /g, "");
  let file_text = "";
  let replyText = "Ubicación " + tweet.user.screen_name + ": '" + tweet.user.location + "'. " + input.input_text + "? Creo que si!";
  const data = await twitController.replyTweet(replyText, tweet.user.is_str);
  console.log("Respondido a mention:" + data.text)
}

const twitController = {
  postTweet: async (text) => {
    return new Promise((resolve, reject) => {
      let params = {
        status: text,
      };
      console.log(params);
      client.post("statuses/update", params, (err2, data) => {
        if (err2) {
          return reject(err2);
        } else {
          return resolve(data);
        }
      });
    });
  },
  postReTweet: async (id) => {
    return new Promise((resolve, reject) => {
      let params = {
        id,
      };
      client.post("statuses/retweet/:id", params, (err, data) => {
        if (err) {
          return reject(err);
        } else {
          return resolve(data);
        }
      });
    });
  },
  getHash: async (hash, count) => {
    return new Promise((resolve, reject) => {
      let params = {
        q: "#" + hash,
        count,
      };
      client.get("search/tweets", params, (error, data) => {
        if (error) {
          return reject(error);
        } else {
          return resolve(data);
        }
      });
    });
  },
  streamCreate: async (account) => {
    return new Promise(async (resolve, reject) => {
        let params = {
            track: account,
        };
        let stream = await client.stream("statuses/filter", params)
        await stream.on("tweet", tweetEvent)
    });
  },
//   streamOn: async () => {
//         try {
//             stream.on("tweet", this.tweetEvent);
//         } catch (error) {
//             console.error(error)
//         }
//   },
  replyTweet: async (text, id) => {
    return new Promise((resolve, reject) => {
        let params = {
          status: text,
          in_reply_to_status_id: id,
        };
        client.post("statuses/update", params, (err2, data) => {
          if (err2) {
            return reject(err2);
          } else {
            return resolve(data);
          }
        });
      });
  }
};

module.exports = twitController;
