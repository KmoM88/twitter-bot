const twiter = require('twit')

const config = require('./config');

const client = new twiter({
    consumer_key: config.consumer_key,
    consumer_secret: config.consumer_secret,
    access_token: config.access_token,
    access_token_secret: config.access_token_secret
  });

module.exports = client;